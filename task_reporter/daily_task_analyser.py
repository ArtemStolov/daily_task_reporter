from argparse import ArgumentParser
from os import getcwd
from os.path import join
from task_reporter.library import xlsx_writer, analyse_transactions, xlsx_reader
import yaml
from dateutil import parser


def main():

    argparser = ArgumentParser(
        description='Инструмент консольной генерации отчетов по результатам выполнения ССЗ.'
    )

    argparser.add_argument('-c', '--config', required=False,
                           default=join(getcwd(), 'daily_task_analyser.yml'))
    argparser.add_argument('-s', '--session', required=False,
                           default=join(getcwd(), 'daily_task_analyser_session.yml'))

    args = argparser.parse_args()
    print(args)

    with open(args.config, 'r', encoding="utf-8") as stream:
        cur_config = yaml.load(stream)

    session = analyse_transactions.TransactionSession()

    try:
        stream = open(args.session, 'r', encoding="utf-8")
        last_session = yaml.load(stream)["session"]
    except FileNotFoundError:
        last_session = ""

    # получаем главную сессию моделирования
    main_session = session.get_main_session(url=cur_config["url"],
                                            login=str(cur_config["login"]),
                                            password=str(cur_config["password"]))

    if main_session == last_session:
        print("Сессия моделирования осталась той же, отчет по ССЗ не будет сгенерирован")
        return

    if main_session is None:
        print("В системе нет принятого расчета! Отчет по ССЗ не будет сгенерирован")
        return
    time_zone = parser.parse(cur_config["rules"]["time_zone"]).tzinfo
    print(time_zone)
    session.read_from_rest(url=cur_config["url"],
                           login=str(cur_config["login"]), password=str(cur_config["password"]),
                           tz=time_zone, session=main_session)

    input_path = cur_config["input_path"]
    output_path = cur_config["control_output_path"]

    for xlsx in cur_config["filenames"]:
        input_xlsx = input_path + xlsx + ".xlsx"
        try:
            daily_task = xlsx_reader.XlsxReadData(xlsx=input_xlsx, title_row=2)
        except FileNotFoundError:
            print("Не найдено ССЗ на подразделение", xlsx)
            continue
        overwrite = True

        for equipment in daily_task.worksheets:
            for row in daily_task.worksheets[equipment]:
                del row["ROW_NUMBER"]
                if row["НОМЕР ЗАКАЗА"] is None:
                    row["НАИМЕНОВАНИЕ"] = "(?) " + row["НАИМЕНОВАНИЕ"]
                    continue
                batch = session.get_batch_with_order_name(row["НОМЕР ЗАКАЗА"].replace("-", "_"))
                if batch is None:
                    operation_now = "ЗАКАЗ ЗАВЕРШЕН"
                else:
                    operation_now = batch.current_operation
                    row["КОЛ-ВО"] = str(row["КОЛ-ВО"]) + "=>" + str(round(batch.initial_batch_size, ndigits=2))
                row["ТЕКУЩАЯ ОПЕРАЦИЯ"] += "=>" + operation_now[0:20]
                if (row["НОМЕР ОПЕРАЦИИ"][0:20] < operation_now[0:20]) and (operation_now != "НЕ ЗАПУЩЕНА"):
                    row["НАИМЕНОВАНИЕ"] = "(+) " + row["НАИМЕНОВАНИЕ"]
                else:
                    row["НАИМЕНОВАНИЕ"] = "(-) " + row["НАИМЕНОВАНИЕ"]
            report_date = str(session.start_time)[0:10] + "_"
            output_xlsx = output_path + "control_" + report_date + xlsx + ".xlsx"
            xlsx_title = "({}) Отчет о выполнении производственного задания: {}".format(session.start_time,
                                                                                        str(equipment))

            xlsx_writer.xlsx_write_data(xlsx=output_xlsx, worksheet=str(equipment)[0:30],
                                        data=daily_task.worksheets[equipment],
                                        title=xlsx_title,
                                        overwrite=overwrite)
            overwrite = False

    overwrite = {}
    all_reports = {}
    for equipment in session.equipment:
        tasks_report = session.daily_tasks_report(date_from=None,
                                                  tasks_period_hours=cur_config["rules"]["tasks_period"],
                                                  equipment_id=equipment)
        if tasks_report:
            department_id = session.get_equipment_with_id(equipment).department_id
            if department_id not in all_reports:
                all_reports[department_id] = {}
            all_reports[department_id][equipment] = tasks_report
            xlsx = str(cur_config["tasks_output"]["actual_path"]) + str(department_id) + ".xlsx"
            if not(department_id in overwrite):
                overwrite[department_id] = True
            xlsx_writer.xlsx_write_data(xlsx=xlsx, worksheet=str(equipment)[0:30],
                                        data=tasks_report, title="ПРОИЗВОДСТВЕННОЕ ЗАДАНИЕ: " + str(equipment),
                                        overwrite=overwrite[department_id])
            xlsx = str(cur_config["tasks_output"]["archive_path"]) + str(session.start_time)[0:10] + "_" + \
                   str(department_id) + ".xlsx"
            xlsx_writer.xlsx_write_data(xlsx=xlsx, worksheet=str(equipment)[0:30],
                                        data=tasks_report, title="ПРОИЗВОДСТВЕННОЕ ЗАДАНИЕ: " + str(equipment),
                                        overwrite=overwrite[department_id])

            overwrite[department_id] = False

    with open(args.session, 'w') as outfile:
        yaml.dump({"session": main_session}, outfile, default_flow_style=False)


if __name__ == "__main__":
    main()

