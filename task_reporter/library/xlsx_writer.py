from openpyxl import Workbook, load_workbook
from openpyxl.styles import Alignment, Border, Side, PatternFill
from time import time


def xlsx_write_data(xlsx, worksheet, data, title=None, overwrite=True):
    """
    Запись словаря в xlsx файл
    :param xlsx: файл, в который записываем
    :type xlsx: str
    :param worksheet: лист, на который пишем
    :type worksheet: str
    :param data: словарь, в который пишем
    :type data: list[dict]
    :param title: Титульник листа, если необходим
    :type title: str
    :return:
    """

    def as_text(value):
        if value is None:
            return ""
        return str(value)
    worksheet = str(worksheet).replace("/", "")
    t = time()
    print("\n \nНачинаем запись файла \"{}\", вкладка \"{}\"".format(xlsx, worksheet))
    if overwrite:
        wb = Workbook()
        ws = wb.active
        ws.title = worksheet
    else:
        wb = load_workbook(xlsx)
        ws = wb.create_sheet(worksheet)

    ws.sheet_properties.tabColor = "1072BA"
    if not data:
        wb.save(xlsx)
        wb.close()
        return

    if title is None:
        pass
    else:
        ws.append([title])
        ws.merge_cells(start_row=1, start_column=1, end_row=1, end_column=len(data[0]))
        currentCell = ws['A1']
        currentCell.alignment = Alignment(horizontal='center')

    if not data:
        wb.save(xlsx)
        wb.close()
        return

    ws.append(key for key in data[0])
    for entry in data:
        ws.append(entry[key] for key in data[0])

    # ширину стобцов делаем по максимальой длине текста

    for column_cells in ws.columns:
        first = True
        length = 0
        for cell in column_cells:
            if first:
                first = False
                continue
            length = max(length, len(as_text(cell.value)) + 1)
        if title is None:
            try:
                ws.column_dimensions[column_cells[0].column_letter].width = length
            except AttributeError:
                ws.column_dimensions[column_cells[0].column].width = length
        else:
            try:
                ws.column_dimensions[column_cells[1].column_letter].width = length
            except AttributeError:
                ws.column_dimensions[column_cells[1].column].width = length

    ws.page_setup.orientation = ws.ORIENTATION_LANDSCAPE
    ws.page_setup.paperSize = ws.PAPERSIZE_A4
    ws.sheet_properties.pageSetUpPr.fitToPage = True

    if title is None:
        ws.print_title_rows = '1:1'
        # фиксируем первую строку и первый столбец
        ws.freeze_panes = ws['B2']
    else:
        ws.print_title_rows = '1:2'
        ws.freeze_panes = ws['B3']

    ws.print_options.gridLines = True
    ws.page_setup.fitToHeight = False

    ws.oddHeader.right.text = str(title) + "Page &[Page] of &N"
    ws.oddHeader.right.size = 14

    from datetime import datetime
    ws.oddHeader.left.text = str(datetime.now())
    ws.oddHeader.left.size = 14

    border = Border(left=Side(border_style='thin', color='DDDDDD'),
                    right=Side(border_style='thin', color='DDDDDD'),
                    top=Side(border_style='thin', color='DDDDDD'),
                    bottom=Side(border_style='thin', color='DDDDDD'))
    alignment = Alignment(vertical='center')
    greyFill = PatternFill(start_color='EEEEEEEE', end_color='EEEEEEEE', fill_type='solid')

    for row in ws:
        for cell in row:
            cell.border = border
            cell.alignment = alignment
            if cell.row % 2 == 0:
                cell.fill = greyFill

    wb.save(xlsx)

    wb.close()

    print("Сохранение на вкладке \"{}\" в файла \"{}\" "
          "успешно завершено за {:.2f} секунд".format(worksheet, xlsx, time()-t))
